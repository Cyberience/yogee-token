Set up the Testing Environment, you must have truffle installed.

```
npm install truffle -g truffle
```

Some dependancies
```
 npm install --save-dev chai
 npm install --save-dev chai-bignumber
 npm install web3@^0.20.6
 yarn add web3@^0.20.6

```

Then in the working folder, suggest a new folder for this

```
truffle init
```

Then you can execute
To compile the code
```
truffle compile
```
For debugging to compiler, this should be enough, the nest step will be deploying and testing the contract. you will need to run a node locally. 
Open a new terminal and run the following command in the root folder of the project.

    truffle develop

To migrate or upload the contract to your engine, as configured in the truffle-config.js
```
truffle migrate
```
To run a test use
```
truffle test
or
watch -c truffle test test/AENToken.test.js
```

An approach I use is to install nodemon or watch to re-run the tests when you save the file.

# Usage


This contract is an ERC1404 compatible contract, that is ERC20 compatible with ERC223 saftey and 1404 regulation support and aditional regulatory control features that include the following features:
* White list Control for
  * Send restriction
  * Receive Restriction
* Freeze function
* Unfreeze Function
* White List Managers

The aditional functionality that the 1404 offers is the ability to validate a restriction befor execution, meaning that you can check if a transaction would be successful before attempting this transaction.
The real benefit of this is gas free action that prevents loss of gas on sending failure. 

This function basicly checks the white list.    
        
        detectTransferRestriction(address from, address to, amount)
        
and returns a state code, 0, 1, 2  or 3.

then you can use the function to get a text readable meaning to the code. 

    messageForTransferRestriction(code)
    
## Best Practice
For future compatibility, it is best to check a transaction can be completed before its execution.
The 1404 standard introduces this concept, with the 2 specific interfaces that returns a code, and a second to give human readable meaning in English.

### Procedure
    1 Call detectTrasnferRestriction, no gas required, its Free
    2 Execute the transaction
    3 And for identify the restriction if denied, you may call the messageForTransferRestriction

# Testing
Truffle is used for running test scripts that can audit the contract. this is done as follows:

    truffle test
    or
    watch -c truffle test test/AENToken.test.js
using watch allows you to edit the tests and see the results as soon as you save the changes.

To test  the connection in case of problems use the following command. Do note the port number.

    curl -H "Content-Type: application/json" -X POST --data '{"id": "1", "jsonrpc":"2.0","method":"net_version","params":[]}' http://localhost:8545
    
To rest the migration use

    curl -H "Content-Type: application/json" -X POST --data '{"id": "1", "jsonrpc":"2.0","method":"net_version","params":[]}' http://localhost:8545
    
# Using this Security Token

This token utilises some hard core features outlined below, but basicly, it has a whitlist and a manager group to be able to add whitelist entries.

## Ownership
The owner has full control over the manager and whitelist members, plus the aditional control over the default rules to apply to the contract, this also includes the freeze functionality within the contract.

## Manager Group
Members of the manager group are able to add and remove white list members. only the owner can modify managers.

## Whitelist

## Freeze / Unfrereze
Freeze function allowws the account to be frozen, this is a function in case of abuse or legal recorse, where if an account has become subject to an investigation, the account can be frozen in place, which will prevent any funds being transfered from the said account. it can still continue to receive though.

## Rules and understanding

due to efficiency, it was necessary to invoke binary logic to lower the gas cost and reduce the amount of programming code to handle white list users and  enforce the control, this was to benefit from binary logic such as OR or AND functions, to determine an accounts privladge.